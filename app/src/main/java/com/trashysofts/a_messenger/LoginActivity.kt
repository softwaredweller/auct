package com.trashysofts.a_messenger

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.login_layout.*
import kotlinx.android.synthetic.main.register_layout.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_layout)

        button_login.setOnClickListener {
            if (textedit_email.text.isNotEmpty() && textedit_password.text.isNotEmpty() && textedit_email.text.contains(
                    "@"
                ) && textedit_email.text.contains("."))
             {

                val email = textedit_email.text.toString()
                val password = textedit_password.text.toString()

                Log.d("LoginActivity", "Attempt login with email/password $email/***")

                FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener{
                        if (it.isSuccessful){
                            Log.d("LoginActivity","Successfully logged in")
                            val intent = Intent(this, MessengerActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)
                            Toast.makeText(this,"Welcome",Toast.LENGTH_SHORT).show()

                        }else{
                            Log.d("LoginActivity","Something went wrong, user isn't initialized")
                            Toast.makeText(this,"Incorrect email or password",Toast.LENGTH_SHORT).show()
                        }
                    }
            } else {
                Log.d("LoginActivity", "Incorrect email or password")
                Toast.makeText(this, "Incorrect email or password", Toast.LENGTH_SHORT).show()
            }
            button_signup.setOnClickListener {
                Log.d("LoginActivity", "Starting RegisterActivity")

                val intent = Intent(this, RegisterActivity::class.java)
                startActivity(intent)
                finish()

            }

        }
        button_signup.setOnClickListener {
            val intent = Intent(this,RegisterActivity::class.java)
            startActivity(intent)
            finish()
        }
        button_help.setOnClickListener {
            Toast.makeText(this,"WE DON'T COOPERATE WITH BOSTON BRANCH",Toast.LENGTH_SHORT).show()
        }


    }
}
