package com.trashysofts.a_messenger

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.trashysofts.a_messenger.R
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.chat_from_row.view.*
import kotlinx.android.synthetic.main.chat_to_row.view.*
import java.sql.Timestamp

class ChatActivity : AppCompatActivity() {

    companion object{
        val TAG = "ChatLog"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)


        val user = intent.getParcelableExtra<User>(NewMessageActivity.USER_KEY)
        Chat_user_name.text = user.username

        setupData()

        chat_send_button.setOnClickListener {
            Log.d(TAG,"User is trying to send message")
            sendMessage()
        }
    }

    class ChatMessage(val id: String,val text: String,val fromId: String, val toId: String, val timestamp: Long)

    private fun sendMessage(){

        val text = chat_text_textedit.text.toString()


        val user = intent.getParcelableExtra<User>(NewMessageActivity.USER_KEY)

        val fromId = FirebaseAuth.getInstance().uid
        val toID = user.uid

        if (fromId == null) return

        val reference = FirebaseDatabase.getInstance().getReference("/messages/").push()

        val chatMessage = ChatMessage(reference.key!!,text,fromId!!,toID,System.currentTimeMillis() / 1000)

        reference.setValue(chatMessage)
            .addOnSuccessListener {
                Log.d(TAG,"Message successfully saved in database with id: ${reference.key}")
            }
        chat_text_textedit.text.clear()
    }

    private fun setupData(){
        val adapter = GroupAdapter<ViewHolder>()

        adapter.add(ChatFromItem("FROM"))
        adapter.add(ChatToItem("TO"))


        recycle_chat_log.adapter = adapter
    }

}


class ChatFromItem(val text: String): Item<ViewHolder>(){

    override fun bind(viewHolder: ViewHolder, position: Int) {

        viewHolder.itemView.chat_output_from.text = text

    }

    override fun getLayout(): Int {
        return R.layout.chat_from_row

    }
}

class ChatToItem(val text: String): Item<ViewHolder>(){

    override fun bind(viewHolder: ViewHolder, position: Int) {

        viewHolder.itemView.chat_output_to.text = text

    }

    override fun getLayout(): Int {
        return R.layout.chat_to_row

    }
}
