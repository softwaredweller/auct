package com.trashysofts.a_messenger

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.provider.MediaStore
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.register_layout.*
import java.util.*

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.register_layout)

        button_signup_reglayout.setOnClickListener {
            toggleRegistration()
        }
        button_help_reg.setOnClickListener {
            Toast.makeText(this,"WE DON'T COOPERATE WITH BOSTON BRANCH",Toast.LENGTH_SHORT).show()
        }

        button_login_reglayout.setOnClickListener {
            Log.d("RegisterActivity", "Starting LoginActivity")

            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()

        }
        button_user_photo.setOnClickListener {
            Log.d("RegisterActivity", "Starting photo selector")

            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }
    }

    var selectedPhotoUri: Uri? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
            Log.d("RegisterActivity", "Photo was selected")

            selectedPhotoUri = data.data

            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, selectedPhotoUri)

            selected_photo_imageview.setImageBitmap(bitmap)

            button_user_photo.alpha = 0f

//            val bitmapDrawable = BitmapDrawable(bitmap)
//            button_user_photo.setBackgroundDrawable(bitmapDrawable)
        }
    }

    private fun toggleRegistration() {
        val email = textedit_email_reg.text.toString()
        val password = textedit_password_reg.text.toString()
        val passwordrepeat = textedit_password_reg_repeat.text.toString()

        if (selectedPhotoUri == null || email.isEmpty() || password.isEmpty() || password != passwordrepeat || !Patterns.EMAIL_ADDRESS.matcher(textedit_email_reg.text.toString()).matches()) {
            Toast.makeText(this, "Something went wrong, check input and select profile picture.", Toast.LENGTH_SHORT).show()
            return
        }

        Log.d("RegisterActivity", "Email is: " + email)
        Log.d("RegisterActivity", "Password is: " + password)

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (!it.isSuccessful) return@addOnCompleteListener

                Log.d(
                    "RegisterActivity",
                    "Successfully created user with uid: ${it.result?.user?.uid}"
                )

                uploadImageToFirebaseStorage()
            }
            .addOnFailureListener {
                Log.d(
                    "RegisterActivity",
                    "Failed to create user: ${it.message}"


                )
                Toast.makeText(this, "${it.message}", Toast.LENGTH_SHORT).show()
            }


    }

    private fun uploadImageToFirebaseStorage() {
        if (selectedPhotoUri == null) return

        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")

        ref.putFile(selectedPhotoUri!!)
            .addOnSuccessListener {
                Log.d("RegisterActivity", "Successfully uploaded image: ${it.metadata?.path}")

                ref.downloadUrl.addOnSuccessListener {
                    Log.d("RegisterActivity", "File Location: $it")

                    saveUserToDatabase(it.toString())
                }
            }
            .addOnFailureListener {
                Log.d("RegisterActivity", "User saving failed")
            }


    }

    private fun saveUserToDatabase(profileImageUrl: String) {
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")

        val user = User(uid, textedit_username_reg.text.toString(), profileImageUrl)
        ref.setValue(user)
            .addOnSuccessListener {
                Log.d("RegisterActivity", "User is saved in Database")

                val intent = Intent(this, MessengerActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)

            }
            .addOnFailureListener {
                Log.d("RegisterActivity", "Failed to add user in Database")
            }

    }


}
@Parcelize
class User(val uid: String, val username: String, val profileImageUrl: String): Parcelable{
    constructor() : this("","","")
}


